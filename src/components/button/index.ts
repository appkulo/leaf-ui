import { Plugin } from "vue";
import LButton from "./LButton.vue";
import { registerComponent } from "../../utils/plugins";

export default {
  install(app) {
    registerComponent(app, LButton, "LButton");
  },
} as Plugin;

export { LButton };
