import { App, DefineComponent, Plugin } from "vue";
import { useProgrammatic } from "./useProgrammatic";

export const registerPlugin = (app: App, plugin: Plugin) => {
  app.use(plugin);
};

export const registerComponent = (
  app: App,
  component: DefineComponent,
  name: string = ""
) => {
  if (name) app.component(name, component);
  else app.component(component.name, component);
};

export const registerComponentProgrammatic = (
  app: App,
  property: string,
  component: any
) => {
  // use composable for unified access to programmatic oruga object
  const { nothingUi, addProgrammatic } = useProgrammatic();

  // add component (manipulates the programmatic oruga object)
  addProgrammatic(property, component);

  // add provide and $oruga (only needed once)
  if (!(app._context.provides && app._context.provides.nothingUi))
    app.provide("nothingUi", nothingUi);
  if (!app.config.globalProperties.$nothingUi)
    app.config.globalProperties.$nothingUi = nothingUi;
};
