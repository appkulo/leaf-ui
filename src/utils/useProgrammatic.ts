// oruga object for programmatic components
const nothingUi: Record<string, any> = {};

// add components to the oruga object
function addProgrammatic(property: string, component: any) {
  nothingUi[property] = component;
}

// composable for internal and external usage
export function useProgrammatic() {
  return { nothingUi, addProgrammatic };
}
