import { App, Plugin } from "vue";
import * as components from "./components";
import { registerPlugin } from "./utils/plugins";
import "./index.css";

export default {
  install(app: App, ..._options: any[]) {
    for (const componentKey in components) {
      registerPlugin(app, (components as any)[componentKey]);
    }

    // Will add programatic component
  },
} as Plugin;

export * from "./components";
